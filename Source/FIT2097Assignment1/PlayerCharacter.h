// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "UObject/Class.h"
#include "Door.h"
#include "GameFramework/Character.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/PrimitiveComponent.h"
#include "Containers/Array.h"
#include "Enemy.h"
#include "EnemyAIController.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class FIT2097ASSIGNMENT1_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()


	UPROPERTY(VisibleAnywhere, Category = "Trigger Capsule")
		class UCapsuleComponent* TriggerCapsule;

public:
	// Sets default values for this character's properties
	APlayerCharacter();

	bool bIsPickingUp = false;

	bool isAttacking = false;
	TArray<FString>Inventory;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	class UPlayerStatComponent* PlayerStatCom;	

	bool isSprinting = false;

	FTimerHandle SprintingHandle;

	UFUNCTION(BlueprintPure)
		FString Returnstamina();
	UFUNCTION(BlueprintPure)
		FString ReturnHealth();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	AEnemyAIController* ThisEnemy;
	ADoor* CurrentDoor;

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	    void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);


		void Die();


		FTimerHandle DestroyHandle;
		void CallDestroy();
public:
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
private:
	void HoriMove(float value);
	void VertMove(float value);

	void HoriRot(float value);
	void VertRot(float value);

	void BeginSprint();
	void EndSprint();

	void HandleSprinting();

	void BeginPickup();
	void EndPickup();
	void ShowInventory();

	void OnAction();

	
	UPROPERTY(EditAnywhere, Category = "Camera")
		UCameraComponent* cam;

};
