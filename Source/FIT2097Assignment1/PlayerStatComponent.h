// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlayerStatComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FIT2097ASSIGNMENT1_API UPlayerStatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerStatComponent();

private:


	

	FTimerHandle StaminaHandle;
	FTimerHandle PoisonHandle;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void RegenerateStamina();

public:	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float stamina; 
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float health;
	void LowerStamina(float value);
	void AddHealth(float value);
	void LowerHealth(float value);
	float GetStamina();
	void ControlSprintingTimer(bool isSprinting);
	float GetHealh();
	void HandlePoison();
	
		
};
