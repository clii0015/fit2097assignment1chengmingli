// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "PlayerStatComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "TimerManager.h"


// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AutoPossessPlayer = EAutoReceiveInput::Player0;

	bUseControllerRotationYaw = false;
	
	cam = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	cam->AttachTo(RootComponent);
	cam->SetRelativeLocation(FVector(0, 0, 40));


	PlayerStatCom = CreateDefaultSubobject<UPlayerStatComponent>("PlayerStatComponent");

	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	TriggerCapsule->InitCapsuleSize(55.0f, 96.0f);
	TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
	TriggerCapsule->SetupAttachment(RootComponent);


	TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this, &APlayerCharacter::OnOverlapBegin);
	TriggerCapsule->OnComponentEndOverlap.AddDynamic(this, &APlayerCharacter::OnOverlapEnd);


	CurrentDoor = NULL;
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(SprintingHandle, this, &APlayerCharacter::HandleSprinting, 0.2f, true);
	
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("Hori", this, &APlayerCharacter::HoriMove); 
	InputComponent->BindAxis("Vert", this, &APlayerCharacter::VertMove);

	InputComponent->BindAxis("HoriRot", this, &APlayerCharacter::HoriRot);
	InputComponent->BindAxis("VertRot", this, &APlayerCharacter::VertRot);


	InputComponent->BindAction("Sprint", IE_Pressed, this, &APlayerCharacter::BeginSprint);
	InputComponent->BindAction("Sprint", IE_Released, this, &APlayerCharacter::EndSprint); 

	InputComponent->BindAction("Interact", IE_Pressed, this, &APlayerCharacter::BeginPickup);
	InputComponent->BindAction("Interact", IE_Released, this, &APlayerCharacter::EndPickup);

	InputComponent->BindAction("Interact", IE_Pressed, this, &APlayerCharacter::OnAction);

	InputComponent->BindAction("ShowInventory", IE_Pressed, this, &APlayerCharacter::ShowInventory);
	
}


//Character basic moving functions
void APlayerCharacter::HoriMove(float value)
{
	if (value)
	{
		if (isSprinting)
		{
			value *= 2;
		}
		else if (PlayerStatCom->GetStamina() <= 10)
		{
			isSprinting = false;
			value /= 2;
		}
	
			AddMovementInput(GetActorRightVector(), value / 2);
		
	
			

		
	}

}

void APlayerCharacter::VertMove(float value)
{
	if (value)
	{
		if (isSprinting)
		{
			value *= 2;
		}
		else if (PlayerStatCom->GetStamina() <= 10)
		{
			isSprinting = false;
			value /=2;
		}

			AddMovementInput(GetActorForwardVector(), value / 2);
		



	}

}
//
//Camera rotation
void APlayerCharacter::HoriRot(float value)
{
	if (value)
	{
		AddActorLocalRotation(FRotator(0, value, 0));

	}
}

void APlayerCharacter::VertRot(float value)
{
	if (value)
	{
		float temp = cam->GetRelativeRotation().Pitch + value;

		if (temp<65 && temp>-65)
		{
			cam->AddLocalRotation(FRotator(value, 0, 0));

		}
		 
	}
}
//
//Sprint function
void APlayerCharacter::BeginSprint()
{
	if (PlayerStatCom->GetStamina() > 10.0f)
	{
		isSprinting = true;
		PlayerStatCom->ControlSprintingTimer(true);
	}
	else if (PlayerStatCom->GetStamina() <= 0.0f)
	{
		PlayerStatCom->ControlSprintingTimer(false);
	}

}

void APlayerCharacter::EndSprint()
{
	
		isSprinting = false;
		PlayerStatCom->ControlSprintingTimer(false);
	
}

void APlayerCharacter::HandleSprinting()
{
	if (isSprinting)
	{
		PlayerStatCom->LowerStamina(2.0f);
		if (PlayerStatCom->GetStamina() <= 0.0f)
			EndSprint();
	}
}

//
//Pick up functions
void APlayerCharacter::BeginPickup()
{
	bIsPickingUp = true;
	
	
}
void APlayerCharacter::EndPickup()
{
	bIsPickingUp = false;
	if (Inventory.Contains("MedKit"))
	{
		PlayerStatCom->AddHealth(50.0f);
		Inventory.RemoveSingle("MedKit");
	}

	
}
//
//Out put inventory through debug messsage
void APlayerCharacter::ShowInventory()
{
	for (auto& Item : Inventory)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f,FColor::Blue, FString::Printf(TEXT("Inventory: %s"), *Item));
	}
}

//Interact with doors
void APlayerCharacter::OnAction()
{
	if (CurrentDoor)
	{
		if (CurrentDoor->bIsDoorLocked == true)
		{
			if (Inventory.Contains("DoorKey"))
			{
				CurrentDoor->Unlock();
			}
		}
		else
		{
			FVector ForwardVector = cam->GetForwardVector();
			CurrentDoor->ToggleDoor(ForwardVector);
		}
	}
}
//Collision Detection
void APlayerCharacter::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		CurrentDoor = Cast<ADoor>(OtherActor);
		
	}

}
void APlayerCharacter::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) 
{
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		CurrentDoor = NULL;
		
	}
}
//Reducing health when reciving damage
float APlayerCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (PlayerStatCom->GetHealh() <= 0.0f)
		return 0.0f;
	
	const float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	
	if (ActualDamage > 0.0f)
	{
		PlayerStatCom->LowerHealth(ActualDamage);
		if (PlayerStatCom->GetHealh()<= 0.0f)
		{
			Die();
		}
	}
	return ActualDamage;
}
//Destroy player character
void APlayerCharacter::Die()
{

	this->GetCharacterMovement()->DisableMovement();
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("You have died"));
	GetWorld()->GetTimerManager().SetTimer(DestroyHandle, this, &APlayerCharacter::CallDestroy, 1.0f, false);
}
//Destroy function
void APlayerCharacter::CallDestroy()
{
	Destroy();
}
//Return the player status as FString
FString APlayerCharacter::Returnstamina()
{
	FString RetString =FString::SanitizeFloat(PlayerStatCom->GetStamina());

	return RetString;
}


FString APlayerCharacter::ReturnHealth()
{
	FString RetString = FString::SanitizeFloat(PlayerStatCom->GetHealh());

	return RetString;
}
