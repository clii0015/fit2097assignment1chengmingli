// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Door.generated.h"

UCLASS()
class FIT2097ASSIGNMENT1_API ADoor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADoor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void CloseDoor(float dt);
	UFUNCTION()
		void OpenDoor(float dt);

	class UStaticMeshComponent* Door;

	UPROPERTY(VisibleAnywhere, Category = "Box Comps")
		UBoxComponent* BoxComp;

	UFUNCTION()
		void ToggleDoor(FVector ForwardVector);

	UPROPERTY(EditAnywhere)
		FString HelpText = FString(TEXT("Press E to open the door"));

	class APlayerCharacter* CuurentPlayerController;

	bool bPlayerIsWithinRange = false;


	void GetPlayer(AActor* Player);
	void Unlock();

	bool Opening;
	bool Closing;
	bool isClosed;

	float DotP;
	float MaxDegree;
	float AddRotation;
	float PosNeg;
	float DoorCurrentRot;

	UPROPERTY(EditAnywhere)
		bool bIsDoorLocked=false;

};
