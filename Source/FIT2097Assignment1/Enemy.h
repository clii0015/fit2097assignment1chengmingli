// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.h"
#include "EnemyAIController.h"
#include "PlayerStatComponent.h"
#include "Enemy.generated.h"

UCLASS()
class FIT2097ASSIGNMENT1_API AEnemy : public ACharacter
{
	GENERATED_BODY()

	
public:
	// Sets default values for this character's properties
	AEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	APlayerCharacter* ThisPlayer;
	AEnemyAIController* ThisEnemy;
	UPlayerStatComponent* PlayerStat;
	bool bWithinRange = false;

	UPROPERTY(VisibleAnywhere, Category = "Trigger Capsule")
		class UCapsuleComponent* TriggerCapsule;

	UFUNCTION()
		void Attack();

	void GetPlayer(AActor* Player);
	void GetEnemy(AActor* Enemy);

	FTimerHandle AttackHandle();
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
		void TriggerEnter(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);


	//UFUNCTION()
		//void TriggerExit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

};
