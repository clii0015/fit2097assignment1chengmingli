// Fill out your copyright notice in the Description page of Project Settings.
#include "Item.h"
#include "Components/PrimitiveComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
AItem::AItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	TBox->GetGenerateOverlapEvents();

	TBox->OnComponentBeginOverlap.AddDynamic(this, &AItem::TriggerEnter);
	TBox->OnComponentEndOverlap.AddDynamic(this, &AItem::TriggerExit);

	RootComponent = TBox;

	SM_TBox = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Box Mesh"));
	SM_TBox->AttachTo(RootComponent);





}



void AItem::GetPlayer(AActor* Player)
{
	MyplayerController = Cast<APlayerCharacter>(Player);
}
// Called when the game starts or when spawned
void AItem::BeginPlay()
{
	Super::BeginPlay();
	
}
//adding the item to inventory
void AItem::Pickup()
{
	
		MyplayerController->Inventory.Add(*ItemName);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("You pick an Item"));
		Destroy();
	
	
}
// Called every frame
void AItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (MyplayerController != NULL)//Nullcheck player controller
	{
		if (MyplayerController->bIsPickingUp && bItemWithinRange)
		{
			Pickup();
		}
	}
}

//Begin over lap function
void AItem::TriggerEnter(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	bItemWithinRange = true;
	
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Press E to pickup the item"));
	
	GetPlayer(OtherActor);
}
//End over lap function
void AItem::TriggerExit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	bItemWithinRange = false;
}
