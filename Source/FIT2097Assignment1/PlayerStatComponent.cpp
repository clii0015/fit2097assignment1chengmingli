// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerStatComponent.h"
#include "PlayerCharacter.h"
#include "TimerManager.h"

// Sets default values for this component's properties
UPlayerStatComponent::UPlayerStatComponent()
{
	stamina = 50.0f;
	health = 100.0f;
}


// Called when the game starts
void UPlayerStatComponent::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimer(StaminaHandle, this, &UPlayerStatComponent::RegenerateStamina, 1.0f, true);
	GetWorld()->GetTimerManager().SetTimer(PoisonHandle, this, &UPlayerStatComponent::HandlePoison, 1.0f, true);
	
}
//regenerate stamina when it is not full
void UPlayerStatComponent::RegenerateStamina() 
{
	if (stamina >= 50.0f)
		stamina = 50.0f;
	else if (stamina < 0.0f)
		stamina = 0.0f;
	else
		++stamina;

}
//Decrease stamina function
void UPlayerStatComponent::LowerStamina(float value)
{
	stamina -=value;
}
//Decrease health function
void UPlayerStatComponent::LowerHealth(float value)
{
	health -= value;
	if (health < 0.0f)
		health = 0.0f;
}
//Increase health function
void UPlayerStatComponent::AddHealth(float value) 
{
	health += value;
	if (health > 100.0f)
		health = 100.0f;
}
//Poison the causes player health to decrease constantly
void UPlayerStatComponent::HandlePoison()
{

	if (APlayerCharacter* Character = Cast<APlayerCharacter>(GetOwner()))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("You are poisoned"));
		Character->TakeDamage(1.0f,FDamageEvent(),Character->GetController(),Character);
	}
}
//Get stamina value
float UPlayerStatComponent::GetStamina()
{
	return stamina;
}
//Get health value
float UPlayerStatComponent::GetHealh() {
	return health;
}

//Control the time count for sprint
void UPlayerStatComponent::ControlSprintingTimer(bool isSprinting)
{
	if (isSprinting)
	{
		GetWorld()->GetTimerManager().PauseTimer(StaminaHandle);
	}
	else
	{
		GetWorld()->GetTimerManager().UnPauseTimer(StaminaHandle);
	}
}