// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "TimerManager.h"
// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0, 5, 0);
	
	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	TriggerCapsule->InitCapsuleSize(60.0f, 100.0f);
	TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
	TriggerCapsule->SetupAttachment(RootComponent);
	

	TriggerCapsule->OnComponentHit.AddDynamic(this, &AEnemy::TriggerEnter);
	//TriggerCapsule->OnComponentEndOverlap.AddDynamic(this, &AEnemy::TriggerExit);


	
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	//GetWorld()->GetTimerManager().SetTimer(AttackHandle, this, &AEnemy::AttackHandle, 5.0f, true);
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (ThisEnemy != NULL)
	{
		if (ThisEnemy->isAttacking && bWithinRange)
		{
			Attack();
		}

	}
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


void AEnemy::GetPlayer(AActor* Player)
{
	ThisPlayer = Cast<APlayerCharacter>(Player);
}

void AEnemy::GetEnemy(AActor* Enemy) 
{
	ThisEnemy = Cast<AEnemyAIController>(Enemy);
}

void AEnemy::Attack()
{

	if (ThisPlayer == Cast<APlayerCharacter>(GetOwner()))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("You are poisoned"));
		ThisPlayer->TakeDamage(30.0f, FDamageEvent(), ThisPlayer->GetController(), ThisPlayer);
	}
		
	
}
void AEnemy::TriggerEnter(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	GetPlayer(OtherActor);
	GetEnemy(OtherActor);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("hit"));
		//bWithinRange = true;
		//ThisEnemy->isAttacking = true;
		//PlayerStat->LowerHealth(1.0f);
		
		
	
	

}
//End over lap function
 