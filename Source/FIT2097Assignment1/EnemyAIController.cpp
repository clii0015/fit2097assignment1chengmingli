// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIController.h"
#include "PlayerCharacter.h"

AEnemyAIController::AEnemyAIController()
{
	PrimaryActorTick.bCanEverTick = true;

	SightConfiguration = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Configuration"));
	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component")));

	SightConfiguration->SightRadius = SightRadius;
	SightConfiguration->LoseSightRadius = SightRadiusLose;
	SightConfiguration->PeripheralVisionAngleDegrees = FieldOfView;
	SightConfiguration->SetMaxAge(SightAge);

	SightConfiguration->DetectionByAffiliation.bDetectEnemies = true;
	SightConfiguration->DetectionByAffiliation.bDetectFriendlies = true;
	SightConfiguration->DetectionByAffiliation.bDetectNeutrals = true;

	GetPerceptionComponent()->SetDominantSense(*SightConfiguration->GetSenseImplementation());
	GetPerceptionComponent()->ConfigureSense(*SightConfiguration);
	GetPerceptionComponent()->OnTargetPerceptionUpdated.AddDynamic(this, &AEnemyAIController::OnSensesUpdated);

	TargetPlayer = nullptr;
}
void AEnemyAIController::BeginPlay()
{
	Super::BeginPlay();

	if (!AIBlackboard)
		return;
	if (!ensure(BehaviorTree))
		return;

	UseBlackboard(AIBlackboard, BlackboardComponent);
	RunBehaviorTree(BehaviorTree);
	
	
	NavigationSystem = Cast<UNavigationSystemV1>(GetWorld()->GetNavigationSystem());
	GenerateNewRandomLocation();


}


void AEnemyAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
}

void AEnemyAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (TargetPlayer)
	{
		BlackboardComponent->SetValueAsVector("PlayerPosition", TargetPlayer->GetActorLocation());
		BlackboardComponent->SetValueAsFloat("Attack", TargetPlayer->TakeDamage(0.1f, FDamageEvent(), TargetPlayer->GetController(), TargetPlayer));
	}
}

FRotator AEnemyAIController::GetControlRotation() const
{

	if (GetPawn()) 
	{
		return FRotator(0, GetPawn()->GetActorRotation().Yaw, 0);

	}
	return FRotator(0, 0, 0);

}

void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	
	
}


void AEnemyAIController::GenerateNewRandomLocation()
{
	if (NavigationSystem)
	{
	
		FNavLocation ReturnLocation;
		NavigationSystem->GetRandomPointInNavigableRadius(GetPawn()->GetActorLocation(), 2000, ReturnLocation);
		BlackboardComponent->SetValueAsVector("PatrolPoint", ReturnLocation.Location);
	}
}
void AEnemyAIController::Attack()
{
	//PlayerStat->LowerHealth(10.0f);
	//APlayerCharacter* Characher = Cast<APlayerCharacter>(TargetPlayer);
	//Characher->TakeDamage(30.0f, FDamageEvent(), Characher->GetController(), Characher);
}



void AEnemyAIController::OnSensesUpdated(AActor* UpdatedActor, FAIStimulus Stimulus) 
 {
	APawn* TemporaryPawn = Cast<APawn>(UpdatedActor);
	
	if (TemporaryPawn && TemporaryPawn->IsPlayerControlled())
	{
	
		if (Stimulus.WasSuccessfullySensed())
		{
			TargetPlayer = TemporaryPawn;
			BlackboardComponent->SetValueAsBool("ChasePlayer", true);
			BlackboardComponent->SetValueAsVector("PlayerPosition", TargetPlayer->GetActorLocation());
			
			
		}
		else
		{
			TargetPlayer = nullptr;
			BlackboardComponent->ClearValue("ChasePlayer");
			BlackboardComponent->ClearValue("CanAttack");
		}
	}
}

