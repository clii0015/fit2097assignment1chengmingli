// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef FIT2097ASSIGNMENT1_PlayerCharacter_generated_h
#error "PlayerCharacter.generated.h already included, missing '#pragma once' in PlayerCharacter.h"
#endif
#define FIT2097ASSIGNMENT1_PlayerCharacter_generated_h

#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_SPARSE_DATA
#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnOverlapBegin); \
	DECLARE_FUNCTION(execReturnHealth); \
	DECLARE_FUNCTION(execReturnstamina);


#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnOverlapBegin); \
	DECLARE_FUNCTION(execReturnHealth); \
	DECLARE_FUNCTION(execReturnstamina);


#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerCharacter(); \
	friend struct Z_Construct_UClass_APlayerCharacter_Statics; \
public: \
	DECLARE_CLASS(APlayerCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT2097Assignment1"), NO_API) \
	DECLARE_SERIALIZER(APlayerCharacter)


#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerCharacter(); \
	friend struct Z_Construct_UClass_APlayerCharacter_Statics; \
public: \
	DECLARE_CLASS(APlayerCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT2097Assignment1"), NO_API) \
	DECLARE_SERIALIZER(APlayerCharacter)


#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerCharacter(APlayerCharacter&&); \
	NO_API APlayerCharacter(const APlayerCharacter&); \
public:


#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerCharacter(APlayerCharacter&&); \
	NO_API APlayerCharacter(const APlayerCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerCharacter)


#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TriggerCapsule() { return STRUCT_OFFSET(APlayerCharacter, TriggerCapsule); } \
	FORCEINLINE static uint32 __PPO__cam() { return STRUCT_OFFSET(APlayerCharacter, cam); }


#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_18_PROLOG
#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_PRIVATE_PROPERTY_OFFSET \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_SPARSE_DATA \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_RPC_WRAPPERS \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_INCLASS \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_PRIVATE_PROPERTY_OFFSET \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_SPARSE_DATA \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_INCLASS_NO_PURE_DECLS \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIT2097ASSIGNMENT1_API UClass* StaticClass<class APlayerCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT2097Assignment1_Source_FIT2097Assignment1_PlayerCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
