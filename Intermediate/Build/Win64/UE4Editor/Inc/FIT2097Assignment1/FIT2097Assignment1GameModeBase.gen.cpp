// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FIT2097Assignment1/FIT2097Assignment1GameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFIT2097Assignment1GameModeBase() {}
// Cross Module References
	FIT2097ASSIGNMENT1_API UClass* Z_Construct_UClass_AFIT2097Assignment1GameModeBase_NoRegister();
	FIT2097ASSIGNMENT1_API UClass* Z_Construct_UClass_AFIT2097Assignment1GameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_FIT2097Assignment1();
// End Cross Module References
	void AFIT2097Assignment1GameModeBase::StaticRegisterNativesAFIT2097Assignment1GameModeBase()
	{
	}
	UClass* Z_Construct_UClass_AFIT2097Assignment1GameModeBase_NoRegister()
	{
		return AFIT2097Assignment1GameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AFIT2097Assignment1GameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFIT2097Assignment1GameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_FIT2097Assignment1,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFIT2097Assignment1GameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "FIT2097Assignment1GameModeBase.h" },
		{ "ModuleRelativePath", "FIT2097Assignment1GameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFIT2097Assignment1GameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFIT2097Assignment1GameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFIT2097Assignment1GameModeBase_Statics::ClassParams = {
		&AFIT2097Assignment1GameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AFIT2097Assignment1GameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFIT2097Assignment1GameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFIT2097Assignment1GameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFIT2097Assignment1GameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFIT2097Assignment1GameModeBase, 235109777);
	template<> FIT2097ASSIGNMENT1_API UClass* StaticClass<AFIT2097Assignment1GameModeBase>()
	{
		return AFIT2097Assignment1GameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFIT2097Assignment1GameModeBase(Z_Construct_UClass_AFIT2097Assignment1GameModeBase, &AFIT2097Assignment1GameModeBase::StaticClass, TEXT("/Script/FIT2097Assignment1"), TEXT("AFIT2097Assignment1GameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFIT2097Assignment1GameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
