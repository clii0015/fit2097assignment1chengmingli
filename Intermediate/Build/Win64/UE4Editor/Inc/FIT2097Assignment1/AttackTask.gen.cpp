// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FIT2097Assignment1/AttackTask.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAttackTask() {}
// Cross Module References
	FIT2097ASSIGNMENT1_API UClass* Z_Construct_UClass_UAttackTask_NoRegister();
	FIT2097ASSIGNMENT1_API UClass* Z_Construct_UClass_UAttackTask();
	AIMODULE_API UClass* Z_Construct_UClass_UBTTaskNode();
	UPackage* Z_Construct_UPackage__Script_FIT2097Assignment1();
// End Cross Module References
	void UAttackTask::StaticRegisterNativesUAttackTask()
	{
	}
	UClass* Z_Construct_UClass_UAttackTask_NoRegister()
	{
		return UAttackTask::StaticClass();
	}
	struct Z_Construct_UClass_UAttackTask_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAttackTask_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBTTaskNode,
		(UObject* (*)())Z_Construct_UPackage__Script_FIT2097Assignment1,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttackTask_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "AttackTask.h" },
		{ "ModuleRelativePath", "AttackTask.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAttackTask_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAttackTask>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAttackTask_Statics::ClassParams = {
		&UAttackTask::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAttackTask_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAttackTask_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAttackTask()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAttackTask_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAttackTask, 931704429);
	template<> FIT2097ASSIGNMENT1_API UClass* StaticClass<UAttackTask>()
	{
		return UAttackTask::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAttackTask(Z_Construct_UClass_UAttackTask, &UAttackTask::StaticClass, TEXT("/Script/FIT2097Assignment1"), TEXT("UAttackTask"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAttackTask);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
