// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT2097ASSIGNMENT1_FIT2097Assignment1GameModeBase_generated_h
#error "FIT2097Assignment1GameModeBase.generated.h already included, missing '#pragma once' in FIT2097Assignment1GameModeBase.h"
#endif
#define FIT2097ASSIGNMENT1_FIT2097Assignment1GameModeBase_generated_h

#define FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_SPARSE_DATA
#define FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_RPC_WRAPPERS
#define FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFIT2097Assignment1GameModeBase(); \
	friend struct Z_Construct_UClass_AFIT2097Assignment1GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AFIT2097Assignment1GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT2097Assignment1"), NO_API) \
	DECLARE_SERIALIZER(AFIT2097Assignment1GameModeBase)


#define FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAFIT2097Assignment1GameModeBase(); \
	friend struct Z_Construct_UClass_AFIT2097Assignment1GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AFIT2097Assignment1GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT2097Assignment1"), NO_API) \
	DECLARE_SERIALIZER(AFIT2097Assignment1GameModeBase)


#define FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFIT2097Assignment1GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFIT2097Assignment1GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFIT2097Assignment1GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFIT2097Assignment1GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFIT2097Assignment1GameModeBase(AFIT2097Assignment1GameModeBase&&); \
	NO_API AFIT2097Assignment1GameModeBase(const AFIT2097Assignment1GameModeBase&); \
public:


#define FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFIT2097Assignment1GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFIT2097Assignment1GameModeBase(AFIT2097Assignment1GameModeBase&&); \
	NO_API AFIT2097Assignment1GameModeBase(const AFIT2097Assignment1GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFIT2097Assignment1GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFIT2097Assignment1GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFIT2097Assignment1GameModeBase)


#define FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_12_PROLOG
#define FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_SPARSE_DATA \
	FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_RPC_WRAPPERS \
	FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_INCLASS \
	FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_SPARSE_DATA \
	FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIT2097ASSIGNMENT1_API UClass* StaticClass<class AFIT2097Assignment1GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT2097Assignment1_Source_FIT2097Assignment1_FIT2097Assignment1GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
