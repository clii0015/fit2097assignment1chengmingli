// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT2097ASSIGNMENT1_GenerateRandomLocationTask_generated_h
#error "GenerateRandomLocationTask.generated.h already included, missing '#pragma once' in GenerateRandomLocationTask.h"
#endif
#define FIT2097ASSIGNMENT1_GenerateRandomLocationTask_generated_h

#define FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_SPARSE_DATA
#define FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_RPC_WRAPPERS
#define FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGenerateRandomLocationTask(); \
	friend struct Z_Construct_UClass_UGenerateRandomLocationTask_Statics; \
public: \
	DECLARE_CLASS(UGenerateRandomLocationTask, UBTTaskNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FIT2097Assignment1"), NO_API) \
	DECLARE_SERIALIZER(UGenerateRandomLocationTask)


#define FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUGenerateRandomLocationTask(); \
	friend struct Z_Construct_UClass_UGenerateRandomLocationTask_Statics; \
public: \
	DECLARE_CLASS(UGenerateRandomLocationTask, UBTTaskNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FIT2097Assignment1"), NO_API) \
	DECLARE_SERIALIZER(UGenerateRandomLocationTask)


#define FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGenerateRandomLocationTask(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGenerateRandomLocationTask) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGenerateRandomLocationTask); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGenerateRandomLocationTask); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGenerateRandomLocationTask(UGenerateRandomLocationTask&&); \
	NO_API UGenerateRandomLocationTask(const UGenerateRandomLocationTask&); \
public:


#define FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGenerateRandomLocationTask(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGenerateRandomLocationTask(UGenerateRandomLocationTask&&); \
	NO_API UGenerateRandomLocationTask(const UGenerateRandomLocationTask&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGenerateRandomLocationTask); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGenerateRandomLocationTask); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGenerateRandomLocationTask)


#define FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_PRIVATE_PROPERTY_OFFSET
#define FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_13_PROLOG
#define FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_PRIVATE_PROPERTY_OFFSET \
	FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_SPARSE_DATA \
	FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_RPC_WRAPPERS \
	FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_INCLASS \
	FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_PRIVATE_PROPERTY_OFFSET \
	FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_SPARSE_DATA \
	FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_INCLASS_NO_PURE_DECLS \
	FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIT2097ASSIGNMENT1_API UClass* StaticClass<class UGenerateRandomLocationTask>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT2097Assignment1_Source_FIT2097Assignment1_GenerateRandomLocationTask_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
