// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
struct FAIStimulus;
#ifdef FIT2097ASSIGNMENT1_EnemyAIController_generated_h
#error "EnemyAIController.generated.h already included, missing '#pragma once' in EnemyAIController.h"
#endif
#define FIT2097ASSIGNMENT1_EnemyAIController_generated_h

#define FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_SPARSE_DATA
#define FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnSensesUpdated);


#define FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnSensesUpdated);


#define FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEnemyAIController(); \
	friend struct Z_Construct_UClass_AEnemyAIController_Statics; \
public: \
	DECLARE_CLASS(AEnemyAIController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT2097Assignment1"), NO_API) \
	DECLARE_SERIALIZER(AEnemyAIController)


#define FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_INCLASS \
private: \
	static void StaticRegisterNativesAEnemyAIController(); \
	friend struct Z_Construct_UClass_AEnemyAIController_Statics; \
public: \
	DECLARE_CLASS(AEnemyAIController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT2097Assignment1"), NO_API) \
	DECLARE_SERIALIZER(AEnemyAIController)


#define FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemyAIController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemyAIController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyAIController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyAIController(AEnemyAIController&&); \
	NO_API AEnemyAIController(const AEnemyAIController&); \
public:


#define FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyAIController(AEnemyAIController&&); \
	NO_API AEnemyAIController(const AEnemyAIController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyAIController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEnemyAIController)


#define FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_PRIVATE_PROPERTY_OFFSET
#define FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_18_PROLOG
#define FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_PRIVATE_PROPERTY_OFFSET \
	FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_SPARSE_DATA \
	FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_RPC_WRAPPERS \
	FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_INCLASS \
	FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_PRIVATE_PROPERTY_OFFSET \
	FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_SPARSE_DATA \
	FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_INCLASS_NO_PURE_DECLS \
	FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIT2097ASSIGNMENT1_API UClass* StaticClass<class AEnemyAIController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT2097Assignment1_Source_FIT2097Assignment1_EnemyAIController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
