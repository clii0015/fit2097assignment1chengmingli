// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FIT2097Assignment1/GenerateRandomLocationTask.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGenerateRandomLocationTask() {}
// Cross Module References
	FIT2097ASSIGNMENT1_API UClass* Z_Construct_UClass_UGenerateRandomLocationTask_NoRegister();
	FIT2097ASSIGNMENT1_API UClass* Z_Construct_UClass_UGenerateRandomLocationTask();
	AIMODULE_API UClass* Z_Construct_UClass_UBTTaskNode();
	UPackage* Z_Construct_UPackage__Script_FIT2097Assignment1();
// End Cross Module References
	void UGenerateRandomLocationTask::StaticRegisterNativesUGenerateRandomLocationTask()
	{
	}
	UClass* Z_Construct_UClass_UGenerateRandomLocationTask_NoRegister()
	{
		return UGenerateRandomLocationTask::StaticClass();
	}
	struct Z_Construct_UClass_UGenerateRandomLocationTask_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGenerateRandomLocationTask_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBTTaskNode,
		(UObject* (*)())Z_Construct_UPackage__Script_FIT2097Assignment1,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateRandomLocationTask_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "GenerateRandomLocationTask.h" },
		{ "ModuleRelativePath", "GenerateRandomLocationTask.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGenerateRandomLocationTask_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGenerateRandomLocationTask>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGenerateRandomLocationTask_Statics::ClassParams = {
		&UGenerateRandomLocationTask::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGenerateRandomLocationTask_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateRandomLocationTask_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGenerateRandomLocationTask()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGenerateRandomLocationTask_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGenerateRandomLocationTask, 103887313);
	template<> FIT2097ASSIGNMENT1_API UClass* StaticClass<UGenerateRandomLocationTask>()
	{
		return UGenerateRandomLocationTask::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGenerateRandomLocationTask(Z_Construct_UClass_UGenerateRandomLocationTask, &UGenerateRandomLocationTask::StaticClass, TEXT("/Script/FIT2097Assignment1"), TEXT("UGenerateRandomLocationTask"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGenerateRandomLocationTask);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
