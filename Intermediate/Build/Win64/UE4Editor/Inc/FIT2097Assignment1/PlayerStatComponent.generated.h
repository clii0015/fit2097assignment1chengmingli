// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT2097ASSIGNMENT1_PlayerStatComponent_generated_h
#error "PlayerStatComponent.generated.h already included, missing '#pragma once' in PlayerStatComponent.h"
#endif
#define FIT2097ASSIGNMENT1_PlayerStatComponent_generated_h

#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_SPARSE_DATA
#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_RPC_WRAPPERS
#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlayerStatComponent(); \
	friend struct Z_Construct_UClass_UPlayerStatComponent_Statics; \
public: \
	DECLARE_CLASS(UPlayerStatComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT2097Assignment1"), NO_API) \
	DECLARE_SERIALIZER(UPlayerStatComponent)


#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUPlayerStatComponent(); \
	friend struct Z_Construct_UClass_UPlayerStatComponent_Statics; \
public: \
	DECLARE_CLASS(UPlayerStatComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT2097Assignment1"), NO_API) \
	DECLARE_SERIALIZER(UPlayerStatComponent)


#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlayerStatComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlayerStatComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerStatComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerStatComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerStatComponent(UPlayerStatComponent&&); \
	NO_API UPlayerStatComponent(const UPlayerStatComponent&); \
public:


#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerStatComponent(UPlayerStatComponent&&); \
	NO_API UPlayerStatComponent(const UPlayerStatComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerStatComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerStatComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPlayerStatComponent)


#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_PRIVATE_PROPERTY_OFFSET
#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_10_PROLOG
#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_PRIVATE_PROPERTY_OFFSET \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_SPARSE_DATA \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_RPC_WRAPPERS \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_INCLASS \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_PRIVATE_PROPERTY_OFFSET \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_SPARSE_DATA \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_INCLASS_NO_PURE_DECLS \
	FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIT2097ASSIGNMENT1_API UClass* StaticClass<class UPlayerStatComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT2097Assignment1_Source_FIT2097Assignment1_PlayerStatComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
